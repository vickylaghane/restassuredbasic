package restAssuredReference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Get_Reference {

	public static void main(String[] args) {
		
//-->	Step 1 : Declare the variable for base URI
		String BaseURI = "https://reqres.in/";
		
//-->	Step 2 : BaseURI
		RestAssured.baseURI = BaseURI ;
		
//-->	Step 3 : Configure request body & trigger the api
		
//		Method 1 : Without using .log().all() method -
//		Will get the response in suppressed format i.e. only Response body parameters will be printed, Headers & other info won't be printed on console.
		
		String responseBody = given()
							  .when().get("api/users/2")
							  .then().extract().response().asString();
		System.out.println(responseBody);
		
		//Fetch StatusCode
		int StatusCode = given().when().get("api/users/2").then().extract().statusCode();
		System.out.println(StatusCode);

		
//		Method 2 : Using .log().all() method -
//		Will get the response in full format i.e. along with Response body parameters, Headers & other info will also be printed on console.
	
//		given().log().all().when().get("api/users/2")
//		.then().log().all().extract().response().asString();
		
//-->	Step 4 : Create Object of JsonPath and parse the response body
		JsonPath json_res = new JsonPath(responseBody);
		
		
		Assert.assertEquals(StatusCode, 200);
	}
}
