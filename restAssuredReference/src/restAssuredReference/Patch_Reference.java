package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Patch_Reference {

	public static void main(String[] args) {

//-->	Step 1 : Declare variable for BaseURI and Request body
		
		String BaseURI = "https://reqres.in/" ;
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}" ;
		
//-->	Step 2 : BaseURI
		
		RestAssured.baseURI = BaseURI;
		
//-->	Step 3 : Configure Request Body and Trigger the API
		
//		Method 1 : Without using .log().all() method -
//		Will get the response in suppressed format i.e. only Response body parameters will be printed, Headers & other info won't be printed on console.
		
		String responseBody = given().header("Content-Type","application/json").body(requestBody)
							  .when().patch("api/users/2")
							  .then().extract().response().asString();
		System.out.println(responseBody);
		
		
//		Method 2 : Using .log().all() method -
//		Will get the response in full format i.e. along with Response body parameters, Headers & other info will also be printed on console.
//		Not necessarily we require all parameters every time.		
		
//		given().header("Content-Type","application/json").body(requestBody).log().all()
//		.when().put("api/users/2")
//		.then().log().all().extract().response().asString();
	
//-->	Step 4 : Create an Object of JsonPath and parse the request body parameters
		JsonPath json_req = new JsonPath (requestBody);
		
		String req_name = json_req.getString("name");
		System.out.println("Request Body parameter 1 : "+req_name);
		
		String req_job = json_req.getString("job");
		System.out.println("Request Body parameter 2 : "+req_job);
		
		// Create Object of LocalDateTime class to generate Date
		LocalDateTime CurrentDate = LocalDateTime.now();
		//System.out.println(CurrentDate);
		
		//Convert Data type of CurrentDate from LocalDateTime to String
		String ExpectedDate = CurrentDate.toString().substring(0,11);
		System.out.println("ExpectedDate :"+ExpectedDate);
		
//-->	Step 5 : Create an Object of JsonPath and parse the response body parameters
		JsonPath json_res = new JsonPath (responseBody);
				
		String res_name = json_res.getString("name");
		System.out.println("Response Body parameter 1 :"+res_name);
		
		String res_job = json_res.getString("job");
		System.out.println("Response Body parameter 2 :"+res_job);
		
		String res_date = json_res.getString("updatedAt").substring(0,11);
		System.out.println("Response Body parameter 3 :"+res_date);
		
//-->	Step 6 : Validate the request and response body parameters
		
		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertEquals(ExpectedDate, res_date);
		
	}

}
