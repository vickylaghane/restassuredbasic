package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_Reference {

	public static void main(String[] args) {
		
//-->	Step 1: Declare the variable for base URI and Request Body
		
		String BaseURI = "https://reqres.in/";
		String Requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}" ;
		
//-->	Step 2 : Assign BaseURI into RestAssured method :
		RestAssured.baseURI = BaseURI;
		
//-->	Step 3 : Configure request body & trigger the api
		
 //		Method 1 : Without using .log().all() method -
 //		Will get the response in suppressed format i.e. only Response body parameters will be printed, Headers & other info won't be printed on console.
		
		String responseBody = given().header("Content-Type","application/json").body(Requestbody)
							  .when().post("api/users")
							  .then().extract().response().asString();
		System.out.println(responseBody);
		
		int StatusCode = given().header("Content-Type", "application/json").body(Requestbody).when().post("api/users").then().extract().statusCode();
	

 //		Method 2 : Using .log().all() method -
 //		Will get the response in full format i.e. along with Response body parameters, Headers & other info will also be printed on console.
			
	/*	given().header("Content-Type","application/json").body(Requestbody).log().all()
		.when().post("api/users")
		.then().log().all().extract().response().asString();
	*/
		
//-->	Step 4 : Create Object of JsonPath and parse the request body parameters.
		JsonPath json_req = new JsonPath(Requestbody);
		
		String req_name = json_req.getString("name");
		System.out.println("Request Body parameter 1: "+req_name);
		
		String req_job = json_req.getString("job");
		System.out.println("Request Body parameter 2: "+req_job);
		
		// Create Object of LocalDateTime class to generate the date
		LocalDateTime CurrentDate = LocalDateTime.now();
		//System.out.println("CurrentDate: "+CurrentDate);
		
		//Convert Data type of CurrentDate from LocalDateTime to String
		String ExpectedDate = CurrentDate.toString().substring(0,11);
		System.out.println("ExpectedDate: "+ExpectedDate);
		

//-->	Step 5 : Create Object of JsonPath and parse the response body
		JsonPath json_res = new JsonPath(responseBody);
		
		String res_name = json_res.getString("name");
		System.out.println("Response Body parameter 1: "+res_name);
		
		String res_job = json_res.getString("job");
		System.out.println("Response Body parameter 2: "+res_job);
		
		String res_id = json_res.getString("id");
		System.out.println("Response Body parameter 3: "+res_id);
		
		String res_date = json_res.getString("createdAt").substring(0,11);
		System.out.println("Response Body parameter 4: "+res_date);
		
		
//-->	Step 6 : Validate the response body parameters
		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(ExpectedDate, res_date);
		Assert.assertEquals(StatusCode, 201);
		
	}

}
